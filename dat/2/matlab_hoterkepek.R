current_dir <- dirname(sys.frame(1)$ofile)
setwd(current_dir)

require(R.matlab)
require(spam)
amd_thermal_maps <- readMat("tmaps.mat")

id <- 19

# Write CSV in R
# write.table(amd_thermal_maps$tmaps[1:392,1:440,id], file = "amd_hoterkep.map",row.names=FALSE, na="",col.names=FALSE, sep=", ")

temperature_map <-amd_thermal_maps$tmaps[1:392,1:440,id]



svg_file <- file("raw_amd_hoterkep.svg","w")



# be kellene olvasni a layout-ot (csv-ben lesz)
layout_components <- read.csv("layout_elemek.csv")


# a color egy 3 elemu karakter vektor lesz
pixel_color <- vector(mode="integer", length=3)
max_color <- 255

temperature_range <- max(temperature_map)-min(temperature_map)

# X es Y koordinata ket elemu numeric vektor lesz 
xy_length <- vector(mode="numeric", length = 2)

# meretek mindig um-ben ertendoek
cell_size <-c(31.3)

x_pitch <- 440
y_pitch <- 392

pixel_size <- 1

picture_grid <- cell_size/pixel_size

picture_width <- x_pitch * picture_grid
picture_height <- y_pitch * picture_grid

writeLines("<?xml version=\"1.0\" standalone=\"no\"?>", con = svg_file)
writeLines("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">", sep="\n", con=svg_file)
writeLines(paste0("<svg width=\"", picture_width, "\" height=\"", picture_height, "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"), sep="\n", con=svg_file)
writeLines("\t<g id=\"thermal_map\">", con=svg_file)

scaled_temperature_map <- (temperature_map - min(temperature_map))/temperature_range

print(paste("max hőmérséklet",max(temperature_map)))
print(paste("min hőmérséklet",min(temperature_map)))

for(x in 1:x_pitch){
  for(y in 1:y_pitch){
    red <- if (scaled_temperature_map[y,x] < 0.3) 0.0 else (1.0/0.7)*(scaled_temperature_map[y,x]-0.3)*max_color
    green <- 0.5 * (scaled_temperature_map[y,x]^4) * max_color
    blue <- 0.5 * (1 - scaled_temperature_map[y,x]^3)*max_color
    writeLines(paste0("\t\t<rect x=\"",(x-1)*picture_grid,"\" y=\"",(y-1)*picture_grid,"\" width=\"",picture_grid,"\" height=\"",picture_grid,"\" style=\"fill:#",
                      format(as.hexmode(as.integer(red)), width=2),format(as.hexmode(as.integer(green)), width=2),format(as.hexmode(as.integer(blue)), width=2),
                      ";stroke-width:0;\"/>"), con=svg_file)
  }
}
writeLines("\t</g>", con=svg_file)
writeLines("\t<g id=\"label_shapes_layer\">", con=svg_file)

for(i in 1:length(layout_components$nev)){
  pos_x <- layout_components[i,"x_pozicio"]/pixel_size
  pos_y <- layout_components[i,"y_pozicio"]/pixel_size
  width <- layout_components[i,"x_meret"]/pixel_size
  height <- layout_components[i,"y_meret"]/pixel_size
  writeLines(paste0("\t\t<rect x=\"", pos_x, "\" y=\"",pos_y,"\" width=\"",width,"\" height=\"",height,"\" style=\"fill:#434343;fill-opacity:0.25;stroke:#cccccc;stroke-width:1;stroke-opacity:0.6\"/>"), con=svg_file)
}

writeLines("</svg>", con=svg_file)
close(svg_file)
