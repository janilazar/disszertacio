%!TEX root = ../disszertacio.tex
\chapref{A~szimulációs tartományok egyesítése}{chap_szimulacios_tartomanyok_egyesitese}
	
	Kutatásom célja egy olyan általános szimulációs módszer kidolgozása volt, amely független az integrált áramkörök tervezési folyamatában alkalmazott modellező nyelvektől és absztrakciós szintektől. Így tetszőleges elvonatkoztatású és nyelvű áramköri modell funkcionális\-/termikus viselkedése vizsgálhatóvá válik.

\secref{Termikus viselkedés modellezése}{sec_termikus_modellezes}

	\imgsrcmetric{fig/4/termikus_gk.pdf}{160mm}{Termikus viselkedés modellezésével bővített GK diagram}{fig_termikus_gk}

	\Aref{fig_termikus_gk}. ábrán a Gajski\-/Kuhn diagram módosított változata látható. A~negyedik ágon az egyes elvonatkoztatási szinteken modellezhető hőmérséklet által befolyásolt működési jellemzőket mutatom be. Magasabb elvonatkoztatási szinteken rendszerszintű tulajdonságok vizsgálhatóak, például a hőmérsékleti működési tartomány vagy a lapkaszintű termikus menedzsment algoritmusok működése.

	A hőmérsékleti működési tartomány a specifikáció alapján határozható meg. Például amíg a kereskedelmi célú processzorokat \SIrange{0}{85}{\celsius} hőmérsékleti tartományra tervezik, a katonai célú eszközöknek a \SIrange{-55}{125}{\celsius} hőmérsékleteken is működöképesnek kell lenniük, űreszközök esetén pedig akár specifikusan egy adott misszió paramétereit figyelembe véve határozzák meg ezt a követelményt. Termikus menedzsment algoritmusok célja, hogy az integrált áramkör működése közben a lapka hőmérséklete ne haladja meg a helyes működéshez szükséges határértékeket. Napjainkban egyre kifinomultabb algoritmusokat implementálnak a processzorgyártók, hogy megakadályozzák a túlmelegedést, és közben növeljék a számítási teljesítményt részterhelés -- kevés programszálat alkalmazó programok -- esetén.

	Alacsonyabb elvonatkoztatási szinteken már több információ áll rendelkezésre az áramkör tényleges fizikai implementációjáról, így lehetőség nyílik a fizikai paraméterek -- például a kapuk késleltetésének és fogyasztásának, vagy a tranzisztorok karakterisztikáinak -- hőmérsékletfüggő modellezésére is.

	A~célkitűzések -- tetszőleges elvonatkoztatású leírás és modellező nyelv támogatása -- miatt az együttes szimulációs módszerek közül kizárólag a relaxációs módszer\fshyp szimulátorcsatolás alkalmazható az új eljárás megvalósításához. A~funkcionális és termikus tartományok összekapcsolásához, az áramköri modell két tartománybeli viselkedésének együttes szimulációjához egymáshoz kell rendelni a modell fizikai megjelenésének egységeit és a viselkedési leírás egyes részeit.

	\secref{Absztrakt interfész kidolgozása}{sec_absztrakt_interfesz}

		A~funkcionális $\left(F\right)$ és termikus $\left(T\right)$ szimulációs tartományok összekapcsolásához bevezettem a \emph{funkcionálismodell\-/komponens} $\left(F_i\right)$, a \emph{disszipációforrás} $\left(p_i\right)$ és \emph{layout alakzat} $\left(L_i\right)$ fogalmakat.


		\begin{description}
			\item[Funkcionálismodell\-/komponens] A~funkcionális modellt $n$ darab diszjunkt részhalmaz, a funkcionálismodell\-/komponensek alkotják. Egy ilyen funkcionálismodell\-/komponens $(F_i)$ hőmérsékletét annak $T_i$ paramétere jellemzi. $F_i$ tulajdonságai formálisan \aref({eq_funkcionalis_komponens}) definíció szerint foglalhatóak össze.

			\begin{equation}
				\label{eq_funkcionalis_komponens}
				\begin{split}
					F = \{F_1 \dots F_n\} \\
					\forall \left( i, j \right),\, i \neq j \enspace F_i \cap F_j = \emptyset \\
					\forall F_i \enspace F_i \mapsto T_i
				\end{split}
			\end{equation}

			\item[Disszipációforrás] A~$p_i$ disszipációforrások tulajdonságai formálisan \aref({eq_disszipacioforras}) definíció szerint foglalhatóak össze. A~disszipációforrások a $P$ disszipációmodell elemei. Minden $F_i$ funkcionális komponenshez pontosan egy disszipációforrás rendelhető, amely annak a komponensnek a teljesítményfelvételét becsli.

			\begin{equation}
				\label{eq_disszipacioforras}
				\begin{gathered}
					P = \{p_1 \dots p_n \} \\
					\forall F_i \subset F \enspace F_i \mapsto p_i \text{ ahol } p_i \in P
				\end{gathered}
			\end{equation}

			\item[Layout alakzat] A~layout része a $T$ termikus modellnek. A~layoutot alkotó $l_{ij}$ téglalapok $\{x,y,z\}_p\footnotemark$\footnotetext{$\{x,y,z\}_p$ pozíciót jellemző számhármas} pozícióval és $\{x,y\}_a\footnotemark$\footnotetext{$\{x,y\}_a$ a felületet jellemző számpár} mérettel rendelkeznek, a fizikai struktúrában elfoglalt pozíció és méret alapján a layout primitívek hőmérséklete meghatározható. Az $L_i$ layout alakzat $m$ darab ilyen téglalap uniója\footnote{Ezáltal több téglalap tartozhat egy adott funkcionális komponeshez, ami lehetővé teszi összetettebb layout alakzatok létrehozását is.}, amely pontosan egy $F_i$ funkcionálismodell\-/komponenshez rendelhető. A layout alakzatok páronként diszjunktak. Ezeket a tulajdonságokat foglalja össze formálisan \aref({eq_layout_alakzat}) definíció.

			\begin{equation}
				\label{eq_layout_alakzat}
				\begin{gathered}
					{\vphantom{\bigcup\limits_{j=1}^{m} l_{ij}}}\{l_{11} \dots l_{nm}\}=L\in T\\
					\forall l_{ij} \enspace l_{ij} \mapsto \{x,y,z\}_p,\{x,y\}_a \\
					L_i = \bigcup\limits_{j=1}^{m} l_{ij} \subset L \\
					\forall \left(i,j\right),\, i \neq j \enspace L_{i} \cap L_{j} = \emptyset \\
					{\vphantom{\bigcup\limits_{j=1}^{m} l_{ij}}} \forall F_i \subset F \enspace F_i \mapsto L_i \text{ ahol } L_i \subset L
				\end{gathered}
			\end{equation}

		\end{description}

		
		\subsubsection{Az~interfész működése}

		\imgsrcmetric{fig/4/keretrendszer_mukodese.pdf}{79mm}{Az~interfész által megvalósított szimulátorcsatolás}{fig_keretrendszer_mukodese}

		A~két tartomány összekapcsolásának célja a funkcionális modell hőmérsékleti információval való bővítése, lehetővé téve a hőmérsékletfüggő viselkedés modellezését. \Aref{fig_keretrendszer_mukodese}. ábrán látható az együttes szimuláció relaxációs módszerrel való megvalósítása a bemutatott fogalomrendszer felhasználásával. A~szimulátorcsatolás ciklusának egy iterációjában a funkcionális modell működése alapján a disszipációmodell kiértékelődik. Ez az információ átadódik a funkcionális modell komponenseihez tartozó $L_i$ layout alakzatoknak. A~layoutot alkotó alakzatoknak ismert a méretük és a pozíciójuk, így a termikus modell a disszipáció alapján meg tudja határozni minden $L_i$ hőmérsékletét\footnote{Mivel a téglalapok rendelkeznek mérettel és pozícióval, így meghatározható a hőmérsékletük. A primitívek uniójával alkotott alakzat hőmérséklete többféleképpen képezhető, például téglalapok felületének arányában vett súlyozott átlagolással, a hőmérsékleti értékek mediánjával, vagy az alakzat középpontjának hőmérsékletével.}. Ezt a hőmérsékletet adja vissza a termikus modell a funkcionális modell komponenseinek.

		Különböző elvonatkoztatási szinteken a disszipációmodell és layout reprezentációja eltérő lehet. Magas elvonatkoztatási szinten a fogyasztást és a területigényt inkább becsülni lehet, mint pontosan modellezni\footnote{Magas elvonatkoztatási szinten a funkcionális modell tetszőleges programozási nyelven megvalósítható. A disszipációmodell és a layout ilyen esetekben a specifikáció és korábbi ismeretek alapján becsülhető és az alkalmazott programozási nyelv felhasználásával a viselkedési leíráshoz illeszthető.}. A fejlesztés előrehaladtával egyre részletesebb információ áll rendelkezésre az áramköri tervről, így a disszipációmodell és a layout is finomítható. Ezeket a modelleket hagyományos tervezési folyamatban is gyakran előállítják a fejlesztés egyéb résztvevői és a tervezés későbbi fázisai számára.
		
\begin{tcolorbox}
\begin{thesis}
	\phantomsection\addcontentsline{toc}{section}{\thethesis.\hspace{.5em}Tézis}\label{modszer_tezis}
	Absztrakt interfészt dolgoztam ki az integrált áramkörök funkcionális és termikus tartománybeli modelljeinek összekapcsolásához. Az~új interfész felhasználható szimulátorcsatolással megvalósított funkcionális\-/termikus együttes szimulációs eljáráshoz, és tetszőleges nyelven implementált, tetszőleges elvonatkoztatású modellel dolgozó szimulátor programhoz illeszthető~\citeS{bognar1, nemeth2, nemeth1, jani1, jani2, jani8}.
\end{thesis}
\end{tcolorbox}

	\secref{Összehasonlítás a korábbi megoldásokkal}{sec_modszer_osszehasonlitas}

		\Aref{sec_absztrakt_interfesz}. fejezetben bemutatott absztrakt interfész fogalmai absztrakciós szinttől és modellező nyelvtől függetlenek, de megfeleltethetőek \aref{fig_termikus_gk}. ábrán szereplő tetszőleges elvonatkoztatási szintű modell elemeinek. Az~interfésszel \aref{subsec_megvalositasok}. fejezetben említett tetszőleges funkcionális\-/termikus együttes szimulációs módszer megvalósítható, \aref{table_interfesz_megfeleltetes}. táblázat foglalja össze az egyes fogalmak jelentését  különböző szimulációs módszerek esetén.

		{\setlength\doublerulesep{.02in}
		\setlength{\arrayrulewidth}{.8pt}
		\begin{longtable}{R{45mm}|m{95mm}}
			\textbf{Szimulációs módszer} &  \multicolumn{1}{c}{\textbf{Az~interfész elemei}} \\ \dhline{2} \endfirsthead
			\textbf{Szimulációs módszer} &  \multicolumn{1}{c}{\textbf{Az~interfész elemei}} \\ \dhline{2} \endhead
			Elektro-termikus  & \Tstrut $F_i$ áramköri elemek (tranzisztorok, ellenállások stb.)\newline
										$p_i$ hálózategyenletek megoldásából számítható\newline
										$L_i$ áramköri elemek layout rajzolata \Bstrut \\ \hline
			Logi-termikus  & \Tstrut $F_i$ logikai kapuk/standard cellák\newline
									 $p_i$ cellakönyvtár fogyasztási adatai\newline
									 $L_i$ a kapuk layout rajzolatai \Bstrut \\ \hline
			Funkcionális-termikus  &\Tstrut $F_i$ összetett funkciójú elemek, IP blokkok \newline
											$p_i$ disszipációt becslő modell (McPAT, Wattch stb.)\newline
											$L_i$ makroblokkok becsült mérete és elrendezése  \\
			 \caption{Különböző absztrakciós szinteken alkalmazott együttes szimulációs módszerek megvalósítása a bemutatott interfésszel}\label{table_interfesz_megfeleltetes} \\
		\end{longtable}}


		A~módszer valós alkalmazásához erre alkalmas programozási nyelven implementálni kell az interfész elemeit és integrálni kell a használni kívánt szimulátorokba. \Aref{chap_framework}. fejezetben a szimulációs módszer egy lehetséges megvalósítását is bemutatom.