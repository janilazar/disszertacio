%!TEX root = ../disszertacio.tex

\chapref{Numerikus módszerek ODE-k megoldásához}{app_numerikus_modszerek}

	Az~értekezésben említett termikus motorok közül a HotSpot a negyedrendű Runge\-/Kutta módszert, a 3D\-/ICE és a SUNRED a hátralépő Euler módszert használja a differenciálegyenlet megoldásához. A SloTh termikus szimulátorban az előrelépő és hátralépő Euler, valamint a trapéz módszeren alapuló numerikus eljárásokat valósítottam meg a

		\begin{equation}
			\begin{split}
				\frac{\mathrm{d}y}{\mathrm{d}t} = f\left( t,y \right)
			\end{split}
		\end{equation}

	\noindent alakú közönséges differenciálegyenletrendszer megoldásához. Ebben a függelékben bemutatom, hogy az egyes módszerek hogyan alkalmazhatóak, milyen lesz a kapott egyenletrendszer.

	\subsubsection{Előrelépő Euler módszer}

		\noindent Előrelépő módszer esetén $y_{(n+1)}$ a már ismert $f\left(t_{(n)},y_{(n)}\right)$ alapján számítható:

		\begin{equation}
			\begin{split}
				\frac{y_{(n+1)} - y_{(n)}}{\Delta t} &= f \left( t_{(n)}, y_{(n)} \right), \\
				y_{(n+1)} &= \Delta t \cdot f \left( t_{(n)}, y_{(n)} \right) + y_{(n)}.
			\end{split}
		\end{equation}

		A fenti összefüggést alkalmazva \aref({eq_linearis_egyenletrendszer2}) egyenletrendszerre egy lépésben meghatározható az új hőmérsékletvektor:

		\begin{equation}
			\begin{split}
			   \frac{\overline{T}_{(n+1)} - \overline{T}_{(n)}}{\Delta t} & = \mathbf{C_{th}^{-1}} \left( \mathbf{G}\overline{T}_{(n)} + \overline{P} \right) \\
			   \overline{T}_{(n+1)} & = \mathbf{C_{th}^{-1}} \left( \mathbf{G}\overline{T}_{(n)} + \overline{P} \right) \Delta t + \overline{T}_{(n)}.
			\end{split}
		\end{equation}

		A fenti egyenlet alapján az $n+1$\-/edik hőmérsékletértékek kiszámításához elégséges két mátrix\-/vektor szorzást és vektor\-/vektor összeadást elvégezni. Az~előrelépő Euler módszer tehát kis számításigénnyel rendelkezik, azonban a numerikus stabilitás érdekében az időléptéket $(\Delta t)$ nagyon kicsire kell megválasztani. A~stabilitás feltétele, hogy \aref({eq_linearis_egyenletrendszer2}) egyenletben a $\mathbf{C_{th}^{-1}} \mathbf{G}$ mátrix összes sajátértékére $\left(\lambda_i\right)$ és az időléptékre $\left(\Delta t\right)$  teljesüljön az alábbi feltétel~\cite{numerical_methods_for_ordinary_differenctial_equations}:

		\begin{equation}
			\label{eq_elorelepo_feltetel}
			\begin{split}
			   \left|1 + \lambda_i \Delta t\right| \leq 1.
			\end{split}
		\end{equation}

		Tehát $\Delta t$-t úgy kell megválasztani, hogy a fenti egyenlőtlenség a legkisebb sajátérték esetén is teljesüljön. A $\lambda_i \Delta t$ szorzat megengedett értékei \aref{fig_elorelepo_stabilitasi_regio}. ábrán fehérrel jelölt körlapon találhatóak. Az~előrelépő Euler módszer kumulatív hibája elsőrendű, vagyis a sorozatos számítások utáni hiba az időléptékkel arányos~\cite{numerical_methods_for_ordinary_differenctial_equations}.

		\imgsrcmetric{fig/b/stabilitas_elorelepo_euler.pdf}{76mm}{Előrelépő Euler stabilitási régiója a komplex számsíkon ábrázolt fehér körlap~\cite{numerical_methods_for_ordinary_differenctial_equations}}{fig_elorelepo_stabilitasi_regio}

	\subsubsection{Hátralépő Euler módszer}

		Hátralépő Euler módszer implicit, $y_{(n+1)}$ kiszámításához a $f\left(t_{(n+1)},y_{(n+1)}\right)$ függvény ismerete szükséges:

		\begin{equation}
			\begin{split}
				\frac{y_{(n+1)} - y_{(n)}}{\Delta t} &= f \left( t_{(n+1)}, y_{(n+1)} \right), \\
				y_{(n+1)} &= \Delta t \cdot f \left( t_{(n+1)}, y_{(n+1)} \right) + y_{(n)}.
			\end{split}
		\end{equation}

		\noindent A fenti összefüggést alkalmazva \aref({eq_linearis_egyenletrendszer2}) egyenletrendszerre adódik a megoldandó egyenletrendszer:

		\begin{equation}
			\begin{split}
			   \frac{\overline{T}_{(n+1)} - \overline{T}_{(n)}}{\Delta t} & = \mathbf{C_{th}^{-1}} \left( \mathbf{G}\overline{T}_{(n+1)} + \overline{P} \right), \\
			   \left(\mathbf{-G}+\frac{\mathbf{C_{th}}}{\Delta t}\right)\overline{T}_{(n+1)} & = \overline{P} + \frac{\mathbf{C_{th}}}{\Delta t}\overline{T}_{(n)}.
			\end{split}
		\end{equation}

		\noindent Az alábbi egyenlet írja le a hátralépő Euler módszer stabilitási feltételét~\cite{numerical_methods_for_ordinary_differenctial_equations} (\ref{fig_hatralepo_stabilitasi_regio}. ábra):

		\begin{equation}
			\label{eq_hatralepo_feltetel}
			\begin{split}
			   \frac{1}{\left|1 - \lambda_i \Delta t\right|} \leq 1,
			\end{split}
		\end{equation}

		\noindent ahol $\lambda_i$ \aref({eq_linearis_egyenletrendszer2}) lineáris egyenletrendszerben szereplő $\mathbf{C_{th}}^{-1} \mathbf{G}$ mátrix sajátértéke.

		\imgsrcmetric{fig/b/stabilitas_hatralepo_euler.pdf}{76mm}{Hátralépő Euler stabilitási régiója a szürke körlap kivételével a teljes komplex számsík }{fig_hatralepo_stabilitasi_regio}

		Az~hátralépő Euler módszer kumulatív hibája elsőrendű, vagyis a sorozatos számítások utáni hiba az időléptékkel arányos~\cite{numerical_methods_for_ordinary_differenctial_equations}.

		\FloatBarrier

	\subsubsection{Trapéz módszer}

		A~trapéz módszer az előrelépő és hátralépő Euler kombinációjának tekinthető, $y_{(n+1)}$ kiszámításához szükséges $f\left(t_{(n)},y_{(n)}\right)$ és $f\left(t_{(n+1)},y_{(n+1)}\right)$ is:

		\begin{equation}
			\begin{split}
				\frac{y_{(n+1)} - y_{(n)}}{\Delta t} &= \frac{1}{2} f \left( t_{(n)}, y_{(n)} \right) + \frac{1}{2} f \left( t_{(n+1)}, y_{(n+1)} \right), \\
				y_{(n+1)} &= \Delta t \cdot \frac{1}{2} f \left( t_{(n)}, y_{(n)} \right) + \Delta t \cdot \frac{1}{2} f \left( t_{(n+1)}, y_{(n+1)} \right) + y_{(n)}.
			\end{split}
		\end{equation}

		\noindent A fenti összefüggést alkalmazva \aref({eq_linearis_egyenletrendszer2}) egyenletrendszerre adódik a megoldandó egyenletrendszer:

		\begin{equation}
			\label{eq_trapez}
			\begin{split}
			   \frac{\overline{T}_{(n+1)} - \overline{T}_{(n)}}{\Delta t} & = \frac{\mathbf{C_{th}^{-1}}}{2} \left( \mathbf{G}\overline{T}_{(n)} + \overline{P} + \mathbf{G}\overline{T}_{(n+1)} + \overline{P} \right), \\
			   \left(\mathbf{-G}+\frac{2\mathbf{C_{th}}}{\Delta t}\right)\overline{T}_{(n+1)} & = \left(\mathbf{G}+\frac{2\mathbf{C_{th}}}{\Delta t}\right)\overline{T}_{(n)} + 2\overline{P}.
			\end{split}
		\end{equation}

		\noindent A következő összefüggés a trapéz szabályon alapuló módszer stabilitási feltétele~\cite{an_introduction_to_numerical_analysis} (\ref{fig_trapez_stabilitasi_regio}. ábra):

		\begin{equation}
			\label{eq_trapez_feltetel}
			\begin{split}
			   \left|\frac{1 + \frac{1}{2}\lambda_i \Delta t}{1 - \frac{1}{2}\lambda_i \Delta t}\right| \leq 1,
			\end{split}
		\end{equation}

		\noindent ahol $\lambda_i$ \aref({eq_linearis_egyenletrendszer2}) lineáris egyenletrendszerben szereplő $\mathbf{C_{th}}^{-1} \mathbf{G}$ mátrix sajátértéke.
				
		\imgsrcmetric{fig/b/stabilitas_trapez.pdf}{76mm}{Trapéz módszer stabilitási régiója a komplex számsík bal oldala}{fig_trapez_stabilitasi_regio}

		A~trapéz módszer kumulatív hibája másodrendű, vagyis a sorozatos számítások utáni hiba az időlépték négyzetével arányos~\cite{an_introduction_to_numerical_analysis}.